# Exploring Earth-like Planets with Deno.Js and TypeScript
The story starts with the kepler spacecraft telescope, this was a state of the art telescope launched into space in 2009 for nearly 10 years.It looked out into space and gather data about planets beyond our solar system. Kepler observed hundreds of thousands of stars and in doing so, discovered over 2000 palanets. And the best part, NASA shared all of the data that kepler gathered, which means in this project and with Deno we are going to take a look! 

### Installing

* git clone https://github.com/letowebdev/Exploring-Habitable-Planets-with-Deno-Js.git
* Ensure you have Deno installed: https://deno.land/
* In the terminal, run: deno run --allow-read mod.ts

## Website

* https://www.youtube.com/watch?v=LOS6z80PaGY

## Resources

* [NASA Exoplanet Archive, https://exoplanetarchive.ipac.caltech.edu/docs/data.html]
* [Habitable Exoplanets Catalog, http://phl.upr.edu/projects/habitable-exoplanets-catalog]
* [Star Masses and Star-Planet Distances for Earth-like Habitability, https://www.liebertpub.com/doi/10.1089/ast.2016.1518]

## Built With

* [Deno.Js]
* [TypeScript]

## Author

* **Zache Abdelatif (Leto)** 
